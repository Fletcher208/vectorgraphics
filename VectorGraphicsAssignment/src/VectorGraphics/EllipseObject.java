package VectorGraphics;

import java.util.ArrayList;

import static java.lang.Math.abs;

public class EllipseObject {
    Double x = 0.0;
    Double y = 0.0;
    Double width = 0.0;
    Double height = 0.0;
    ArrayList<Double> shapeValues = new ArrayList<Double>();


    /**
     * allows for ellipseobject to be constructed from a method
     * Messed up initial construction for a constructor and made a method
     * @param x - x coordinate
     * @param y - y coordinate
     * @param width - width coordinate
     * @param height - height coordinate
     */
    public void EllipseObject(Double x,Double y, Double width, Double height)
    {

        shapeValues = new ArrayList<Double>();
        this.x = x;
        this.y = y;
        setWidth(width);
        setHeight(height);
    }

    /**
     *
     * @param x sets x value for ellipse
     */
    public void setX(Double x){

            this.x = x;



    }

    /**
     *
     * @param y sets y value for ellipse
     */
    public void setY(Double y){

        this.y = y;
    }

    /**
     *
     * @param width sets width value for ellipse, and checks if value is negative if negative it swaps the coordinates of x and width
     */
    public void setWidth(Double width){

            this.width = width - x;
            if(this.width < 0){
                x = abs(x + this.width);
                this.width = abs(this.width);
        }

    }
    /**
     *
     * @param height sets height value for ellipse, and checks if value is negative if negative it swaps the coordinates of y and height
     */
    public void setHeight(Double height){
        this.height = height-y;
        if(this.height < 0){
            y = abs(y + this.height);
            this.height = abs(this.height);
        }
    }

    /**
     * adds the values to an array for reading
     * @return return the array for reading.
     */
    public ArrayList<Double> returnShapeValues(){
        shapeValues.add(this.x);
        shapeValues.add(this.y);
        shapeValues.add(this.width);
        shapeValues.add(this.height);
        return shapeValues;
    }
}
