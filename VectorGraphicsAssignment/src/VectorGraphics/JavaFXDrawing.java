package VectorGraphics;
import javafx.scene.canvas.GraphicsContext;

import javafx.scene.paint.Color;

import java.util.ArrayList;

import static java.lang.Math.abs;

public class JavaFXDrawing {


    /**
     * creates the stroke and fill of a rectangle
     * @param arr is the list of elements to create the rectangle from
     * @param GC is the Graphics context so it can draw to the canvas
     */
    public void Rectangle(ArrayList<Double> arr, GraphicsContext GC){

        GC.fillRect(arr.get(0),arr.get(1), arr.get(2), arr.get(3) );
        GC.strokeRect(arr.get(0),arr.get(1), arr.get(2), arr.get(3));

    }
    /**
     * creates the stroke of a Line
     * @param arr is the list of elements to create the Line from
     * @param GC is the Graphics context so it can draw to the canvas
     */
    public void Line(ArrayList<Double> arr, GraphicsContext GC){


        GC.strokeLine(arr.get(0),arr.get(1), arr.get(2), arr.get(3));


    }
    /**
     * creates the stroke and fill of a Ellipse
     * @param arr is the list of elements to create the Ellipse from
     * @param GC is the Graphics context so it can draw to the canvas
     */
    public void Ellipse(ArrayList<Double> arr, GraphicsContext GC){

            GC.fillOval(arr.get(0),arr.get(1), arr.get(2), arr.get(3));
            GC.strokeOval(arr.get(0),arr.get(1), arr.get(2), arr.get(3));

    }
    /**
     * creates the stroke and fill of a Plot
     * decided to make plot a oval so it is more visible instead of a tiny dot.
     * @param arr is the list of elements to create the Plot from
     * @param GC is the Graphics context so it can draw to the canvas
     */
    public void Plot(ArrayList<Double> arr, GraphicsContext GC){

        GC.strokeOval(arr.get(0),arr.get(1), 1 , 1);
        GC.fillOval(arr.get(0),arr.get(1), 1 , 1);

    }
    /**
     * creates the stroke and fill of a Polygon
     * @param arrX is the list of X elements to create the Polygon from
     * @param arrY is the list of Y elements to create the Polygon from
     * @param GC is the Graphics context so it can draw to the canvas
     */
    public void Polygon(double[] arrX, double[] arrY, GraphicsContext GC){

            GC.fillPolygon(arrX,arrY, arrX.length);
            GC.strokePolygon(arrX,arrY, arrX.length);

    }
    /**
     * creates the stroke and fill of a rectangle when reading from a file
     * checks if width or height is less then x or y then reverses them and makes width and height positive allowing for all types of rectangles to be drawn
     * @param arr is the list of elements to create the rectangle from
     * @param GC is the Graphics context so it can draw to the canvas
     */
    public void readRectangle(ArrayList<Double> arr, GraphicsContext GC){
        System.out.println(arr);
        arr.set(2, arr.get(2) - arr.get(0));
        arr.set(3, arr.get(3) - arr.get(1));

        System.out.println(arr);
        if(arr.get(3) < 0){
            arr.set(1,abs(arr.get(1) + arr.get(3)));
            arr.set(3, abs(arr.get(3)));
        }

        if(arr.get(2) < 0){
            arr.set(0,abs(arr.get(0) + arr.get(2))) ;
            arr.set(2, abs(arr.get(2)));
        }
        System.out.println(arr);

        GC.fillRect(arr.get(0),arr.get(1), arr.get(2), arr.get(3) );
        GC.strokeRect(arr.get(0),arr.get(1), arr.get(2) , arr.get(3));

    }
    /**
     * creates the stroke of line for reading (isnt different from previous method)
     * @param arr is the list of elements to create the rectangle from
     * @param GC is the Graphics context so it can draw to the canvas
     */
    public void readLine(ArrayList<Double> arr, GraphicsContext GC){


        GC.strokeLine(arr.get(0),arr.get(1), arr.get(2), arr.get(3));


    }
    /**
     * creates the stroke and fill of a ellipse when reading from a file
     * checks if width or height is less then x or y then reverses them and makes width and height positive allowing for all types of ellipse to be drawn
     * @param arr is the list of elements to create the ellipse from
     * @param GC is the Graphics context so it can draw to the canvas
     */
    public void readEllipse(ArrayList<Double> arr, GraphicsContext GC){
        System.out.println(arr);
        arr.set(2, arr.get(2) - arr.get(0));
        arr.set(3, arr.get(3) - arr.get(1));

        System.out.println(arr);
        if(arr.get(3) < 0){
            arr.set(1,abs(arr.get(1) + arr.get(3)));
            arr.set(3, abs(arr.get(3)));
        }

        if(arr.get(2) < 0){
            arr.set(0,abs(arr.get(0) + arr.get(2))) ;
            arr.set(2, abs(arr.get(2)));
        }

    GC.fillOval(arr.get(0), arr.get(1), arr.get(2), arr.get(3));
    GC.strokeOval(arr.get(0), arr.get(1), arr.get(2), arr.get(3));


    }
    /**
     * creates the stroke and fill of plot for reading (isnt different from previous method)
     * decided to make plot a oval so it is more visible instead of a tiny dot.
     * @param arr is the list of elements to create the plot from
     * @param GC is the Graphics context so it can draw to the canvas
     */
    public void readPlot(ArrayList<Double> arr, GraphicsContext GC){

        GC.strokeOval(arr.get(0),arr.get(1), 1 , 1);
        GC.fillOval(arr.get(0),arr.get(1), 1 , 1);

    }
    /**
     * creates the stroke and fill of a Polygon
     * @param arrX is the list of X elements to create the Polygon from
     * @param arrY is the list of Y elements to create the Polygon from
     * @param GC is the Graphics context so it can draw to the canvas
     */
    public void readPolygon(double[] arrX, double[] arrY, GraphicsContext GC){

        GC.fillPolygon(arrX,arrY, arrX.length);
        GC.strokePolygon(arrX,arrY, arrX.length);

    }


}
