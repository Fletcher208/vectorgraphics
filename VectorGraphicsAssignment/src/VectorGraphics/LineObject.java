package VectorGraphics;

import java.util.ArrayList;

import static java.lang.Math.abs;

public class LineObject {
    Double x = 0.0;
    Double y = 0.0;
    Double x2 = 0.0;
    Double y2 = 0.0;
    ArrayList<Double> shapeValues = new ArrayList<Double>();

    /**
     *  allows for LineObject to be constructed from a method
     *  Messed up initial construction for a constructor and made a method
     * @param x - x coordinate
     * @param y - y coordinate
     * @param x2 - x2 coordinate
     * @param y2 - y2 coordinate
     */
    public void LineObject(Double x, Double y, Double x2, Double y2){
        shapeValues = new ArrayList<Double>();
        this.x = x;
        this.y = y;
        this.x2 = x2;
        this.y2 = y2;
    }

    /**
     *
     * @param x sets X value for line object
     */
    public void setX(Double x){
        this.x = x;
    }

    /**
     *
     * @param y Sets Y value for Line object
     */
    public void setY(Double y){
        this.y = y;
    }

    /**
     *
     * @param x2 sets x2 value for line object
     */
    public void setX2(Double x2){
        this.x2 = x2;

    }

    /**
     *
     * @param y2 sets y2 value for line object
     */
    public void setY2(Double y2){
        this.y2 = y2;

    }

    /**
     *
     * @return returns the list in type double
     */
    public ArrayList<Double> returnShapeValues(){
        shapeValues.add(this.x);
        shapeValues.add(this.y);
        shapeValues.add(this.x2);
        shapeValues.add(this.y2);
        return shapeValues;
    }
}
