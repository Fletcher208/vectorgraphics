package VectorGraphics;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.stage.FileChooser;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Border;


import java.io.File;
import java.io.IOException;

import static java.lang.Math.abs;

public class Main extends Application {
    /**
     * initialization for other classes for use in the javafx interface (start method)
     */
    private final String[] DrawTool = {"nothing"};
    private ReadFile readFile = new ReadFile();
    private UndoSave undosave = new UndoSave();
    private JavaFXDrawing drawing = new JavaFXDrawing();
    private RectangleObject rectangle = new RectangleObject();
    private LineObject line = new LineObject();
    private PlotObject plot = new PlotObject();
    private EllipseObject ellipse = new EllipseObject();
    public Canvas canvas = new Canvas();
    FileChooser fileChooser = new FileChooser();
    String path;


    /**
     *  steps up the main screen and shows it on program start
     * @param primaryStage primary stage for GUI application
     * @throws Exception Throw exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{


        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("VEC files (*.vec)", "*.vec");
        fileChooser.getExtensionFilters().add(extFilter);
        File selectedFile = new File("dump.vec");
        path = selectedFile.getCanonicalPath();
        undosave.setPath(path);
        System.out.println(path);


        GraphicsContext GC = canvas.getGraphicsContext2D();

        Group sceneRoot = new Group();
        Scene scene = new Scene(sceneRoot,1440,900);


        // renames application title
        primaryStage.setTitle("Vector Graphics");




        //Setup canvas
        canvas.setHeight(720);
        canvas.setWidth(1260);
        canvas.setLayoutX(115);
        canvas.setLayoutY(100);

        // draw banners for button elements
        Rectangle tRect = new Rectangle(0,0,1920,80);
        tRect.setFill(Color.GREY);

        Rectangle sRect = new Rectangle(0,310,100,350);
        sRect.setFill(Color.LIGHTGREY);

        // draws outline for drawing canvas
        Rectangle cborder = new Rectangle(115,100,1260,720);
        cborder.setStroke(Color.BLACK);
        cborder.setFill(Color.WHITE);

        // create visual elements
        // create Load button
        Button buttonLoad = new Button("Load File");
        buttonLoad.setOnMouseClicked((new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {

                File selectedFile = fileChooser.showOpenDialog(primaryStage);
                path = selectedFile.getAbsolutePath();

                readFile.readLines(path, GC, undosave);

            }
        }));

        // Save button
        Button buttonSave = new Button("Save File");

        buttonSave.setOnMouseClicked((new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                File selectedFile = fileChooser.showSaveDialog(primaryStage);
                path = selectedFile.getAbsolutePath();
                undosave.setPath(path);

                try {
                    undosave.writeToFile();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }));

        // Zoom in button
        Button buttonZin = new Button("Zoom in");

        buttonZin.setOnMouseClicked((new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                canvas.setScaleX(1);
                canvas.setScaleY(1);


            }
        }));

        // Zoom out button
        Button buttonZout = new Button("Zoom out");

        buttonZout.setOnMouseClicked((new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {

                canvas.setScaleX(0.5);
                canvas.setScaleY(0.5);


            }
        }));

        // create top banner container for top buttons with spacing

            HBox tBanner = new HBox();
            tBanner.getChildren().addAll(buttonLoad,buttonSave,buttonZin, buttonZout);
            tBanner.setBorder(Border.EMPTY);
            tBanner.setPadding(new Insets(20,20,40,5));
            tBanner.setSpacing(10);




        // creates vertical buttons on left-hand side
        // Undo button
        Button buttonUndo = new Button("Undo");
        buttonUndo.setLayoutX(020);
        buttonUndo.setLayoutY(260);
        scene.setOnKeyPressed((keyEvent -> {
            if(keyEvent.isControlDown() && keyEvent.getCode() == KeyCode.Z){
                undosave.undo();
                try {
                    undosave.writeToFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                readFile.readLines(path,GC,undosave);
            }
        }));

        buttonUndo.setOnMouseClicked((new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                undosave.undo();
                try {
                    undosave.writeToFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                readFile.readLines(path,GC,undosave);
            }
        }));

        // creates vertical buttons on left-hand side
        // Redo button
        Button buttonRedo = new Button("Redo");
        buttonRedo.setLayoutX(020);
        buttonRedo.setLayoutY(160);
        buttonRedo.setOnMouseClicked((new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                undosave.redo();
                try {
                    undosave.writeToFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                readFile.readLines(path,GC,undosave);

            }
        }));

        // Draw Rectangle button
        Button buttonRectangle = new Button("Rectangle");
        buttonRectangle.setLayoutX(020);
        buttonRectangle.setLayoutY(360);
        buttonRectangle.setOnMouseClicked((new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("Rectangle");
               // allows rectangle creation
                DrawTool[0] = "rectangle";
            }
        }));

        // Draw Ellipse button
        Button buttonEllipse = new Button("Ellipse");
        buttonEllipse.setLayoutX(020);
        buttonEllipse.setLayoutY(460);
        buttonEllipse.setOnMouseClicked((new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                DrawTool[0] = "Ellipse";
            }
        }));

        // Draw Line button
        Button buttonLine = new Button("Line");
        buttonLine.setLayoutX(020);
        buttonLine.setLayoutY(560);
        buttonLine.setOnMouseClicked((new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                DrawTool[0] = "line";
            }
        }));

        // Plot tool button
        Button buttonPlot = new Button("Plot");
        buttonPlot.setLayoutX(020);
        buttonPlot.setLayoutY(660);
        buttonPlot.setOnMouseClicked((new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                DrawTool[0] = "plot";
            }
        }));

        // Draw Polygon button
        Button buttonPolygon = new Button("Polygon");
        buttonPolygon.setLayoutX(020);
        buttonPolygon.setLayoutY(760);
        buttonPolygon.setOnMouseClicked((new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                DrawTool[0] = "Polygon";
            }
        }));

        // create side banner container for vertical buttons

        VBox sBanner = new VBox();
        sBanner.getChildren().addAll(buttonRedo,buttonUndo,buttonRectangle,buttonEllipse, buttonLine, buttonPlot, buttonPolygon);
        sBanner.setBorder(Border.EMPTY);
        sBanner.setPadding(new Insets(330,5,10,5));
        sBanner.setSpacing(20);

        EventHandler<MouseEvent> eventHandlerClicked = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                GC.setStroke(Color.BLACK);
                GC.setFill(Color.TRANSPARENT);

                    if (e.getEventType().toString().equalsIgnoreCase("MOUSE_PRESSED")) {
                        Double x = e.getX();
                        Double y = e.getY();
                        if (DrawTool[0].equalsIgnoreCase("rectangle")) {
                            rectangle = new RectangleObject();
                            rectangle.setX(x);
                            rectangle.setY(y);
                            System.out.println(rectangle.x);

                        }

                        if(DrawTool[0].equalsIgnoreCase("Line")){
                            line = new LineObject();
                            line.setX(x);
                            line.setY(y);
                        }

                        if(DrawTool[0].equalsIgnoreCase("Plot")){
                            plot = new PlotObject();
                            plot.setX(x);
                            plot.setY(y);
                        }

                        if(DrawTool[0].equalsIgnoreCase("Ellipse")){
                            ellipse = new EllipseObject();
                            ellipse.setX(x);
                            ellipse.setY(y);
                        }

                    }
            }
        };

        EventHandler<MouseEvent> eventHandlerReleased = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getEventType().toString().equalsIgnoreCase("MOUSE_RELEASED")) {
                    Double x = e.getX();
                    Double y = e.getY();
                    if(DrawTool[0].equalsIgnoreCase("rectangle")) {
                        undosave.addToList("Rectangle" + " " + rectangle.x/canvas.getWidth() + " " + rectangle.y/canvas.getHeight() + " " + x/canvas.getWidth() + " "+ y/canvas.getHeight());
                        System.out.println(rectangle.x);
                        rectangle.setWidth(x);
                        rectangle.setHeight(y);
                        drawing.Rectangle(rectangle.returnShapeValues(), GC);



                    }
                    if(DrawTool[0].equalsIgnoreCase("Line")){
                        line.setX2(x);
                        line.setY2(y);
                        drawing.Line(line.returnShapeValues(),GC);
                        undosave.addToList("Line" + " " + line.x/canvas.getWidth() + " " + line.y/canvas.getHeight() + " " + line.x2/canvas.getWidth() + " "+ line.y2/canvas.getHeight());
                    }
                    if(DrawTool[0].equalsIgnoreCase("Plot")){
                        drawing.Plot(plot.returnShapeValues(),GC);
                        undosave.addToList("Plot" + " " + plot.x/canvas.getWidth() + " " + plot.y/canvas.getHeight() + " " + plot.width/canvas.getWidth() + " "+ plot.height/canvas.getHeight());
                    }
                    if(DrawTool[0].equalsIgnoreCase("Ellipse")){
                        undosave.addToList("Ellipse" + " " + ellipse.x/canvas.getWidth() + " " + ellipse.y/canvas.getHeight() + " " + x/canvas.getWidth() + " "+ y/canvas.getHeight());
                        ellipse.setWidth(x);
                        ellipse.setHeight(y);
                        drawing.Ellipse(ellipse.returnShapeValues(),GC);

                    }
                    undosave.historylist.clear();
                }
            }
        };



        //Registering the event filter
        canvas.addEventFilter(MouseEvent.MOUSE_PRESSED,eventHandlerClicked);
        canvas.addEventFilter(MouseEvent.MOUSE_RELEASED,eventHandlerReleased);





        // adding to grouping and setting the scene
        sceneRoot.getChildren().addAll(sRect,tRect,sBanner, tBanner, cborder);

        sceneRoot.getChildren().add(canvas);

        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);


    }
}
