package VectorGraphics;

import java.util.ArrayList;

import static java.lang.Math.abs;

public class RectangleObject
{

    Double x = 0.0;
    Double y = 0.0;
    Double width = 0.0;
    Double height = 0.0;
    ArrayList<Double> shapeValues = new ArrayList<Double>();



    /**
     *  allows for RectangleObject to be constructed from a method
     *  Messed up initial construction for a constructor and made a method
     * @param x - x coordinate
     * @param y - y coordinate
     * @param width - width coordinate
     * @param height - height coordinate
     */
    public void RectangleObject(Double x,Double y, Double width, Double height)
    {

        shapeValues = new ArrayList<Double>();
        this.x = x;
        this.y = y;
        setWidth(width);
        setHeight(height);
    }


    /**
     *
     * @param x - sets X value
     */
    public void setX(Double x){
        this.x = x;
    }

    /**
     *
     * @param y sets y value
     */
    public void setY(Double y){
        this.y = y;
    }

    /**
     *
     * @param width sets width value
     */
    public void setWidth(Double width){
        this.width = width - x;
        if(this.width < 0){
            x = abs(x + this.width);
            this.width = abs(this.width);
        }
    }

    /**
     *
     * @param height sets height value
     */
    public void setHeight(Double height){
        this.height = height-y;
        if(this.height < 0){
            y = abs(y + this.height);
            this.height = abs(this.height);
        }
    }

    /**
     *
     * @return return all values in a double type list
     */
    public ArrayList<Double> returnShapeValues(){

        shapeValues.add(this.x);
        shapeValues.add(this.y);
        shapeValues.add(this.width);
        shapeValues.add(this.height);
        return shapeValues;
    }






}


