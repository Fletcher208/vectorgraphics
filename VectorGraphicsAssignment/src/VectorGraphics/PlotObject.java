package VectorGraphics;

import java.util.ArrayList;

import static java.lang.Math.abs;

public class PlotObject {

    Double x = 0.0;
    Double y = 0.0;
    Double width = 1.0;
    Double height = 1.0;
    ArrayList<Double> shapeValues = new ArrayList<Double>();

    /**
     *  allows for PlotObject to be constructed from a method
     *  Messed up initial construction for a constructor and made a method
     * @param x - x coordinate
     * @param y - y coordinate
     */
    public void PlotObject(Double x, Double y){
        shapeValues = new ArrayList<Double>();
        this.x = x;
        this.y = y;
    }

    /**
     *
     * @param x Sets x value for Plot Object
     */
    public void setX(Double x){
        this.x = x;
    }

    /**
     *
     * @param y sets y value for plot object
     */
    public void setY(Double y){
        this.y = y;
    }

    /**
     *
     * @return adds values to type double list
     */
    public ArrayList<Double> returnShapeValues(){
        shapeValues.add(this.x);
        shapeValues.add(this.y);
        shapeValues.add(this.width);
        shapeValues.add(this.height);
        return shapeValues;
    }

}
