package VectorGraphics;
import static org.junit.jupiter.api.Assertions.assertEquals;


import javafx.scene.canvas.GraphicsContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TestCases {


    private RectangleObject rectangle = new RectangleObject();
    private LineObject line = new LineObject();
    private PlotObject plot = new PlotObject();
    private EllipseObject ellipse = new EllipseObject();

    @BeforeEach
    public void shapeSetup(){
        rectangle.RectangleObject(5.0,5.0,-5.0,-5.0);
        line.LineObject(5.0,5.0,30.0,25.0);
        plot.PlotObject(5.0,10.0);
        ellipse.EllipseObject(23.0,26.0,7.0,-3.0);
    }

    /**
     * Tests for shape being drawn from bottom right to top left
     * where the width and height coordinate would be higher then the x and y
     */
    @Test
    public void rectangleTest(){
        assertEquals(5, rectangle.x);
        assertEquals(5, rectangle.y);
        assertEquals(10, rectangle.width);
        assertEquals(10, rectangle.height);
    }

    @Test
    public void rectangleArray(){

        ArrayList<Double> actual = new ArrayList<Double>();
        actual.add(5.0);
        actual.add(5.0);
        actual.add(10.0);
        actual.add(10.0);

        assertEquals(actual,rectangle.returnShapeValues());
    }

    /**
     * line consists of 2 sets of coordinates this will make sure it assigns the coordinates correctly
     */
    @Test
    public void lineTest(){

        assertEquals(5, line.x);
        assertEquals(5, line.y);
        assertEquals(30, line.x2);
        assertEquals(25, line.y2);
    }

    @Test
    public void lineArray(){
        ArrayList<Double> actual = new ArrayList<Double>();
        actual.add(5.0);
        actual.add(5.0);
        actual.add(30.0);
        actual.add(25.0);

        assertEquals(actual,line.returnShapeValues());
    }

    @Test
    public void plotTest(){

        assertEquals(5, plot.x);
        assertEquals(10, plot.y);
    }

    @Test
    public void plotArray(){
        ArrayList<Double> actual = new ArrayList<Double>();
        actual.add(5.0);
        actual.add(10.0);
        // width and height as plot is an ellipse
        actual.add(1.0);
        actual.add(1.0);

        assertEquals(actual,plot.returnShapeValues());
    }

    @Test
    public void ellipseTest(){


        assertEquals(7, ellipse.x);
        assertEquals(3, ellipse.y);
        assertEquals(16, ellipse.width);
        assertEquals(29, ellipse.height);
    }

    @Test
    public void ellipseArray(){
        ArrayList<Double> actual = new ArrayList<Double>();
        actual.add(7.0);
        actual.add(3.0);
        actual.add(16.0);
        actual.add(29.0);

        assertEquals(actual,ellipse.returnShapeValues());
    }





}
