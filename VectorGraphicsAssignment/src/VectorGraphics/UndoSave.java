package VectorGraphics;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.ArrayList;

public class UndoSave {
ArrayList<String> list = new ArrayList<String>();
ArrayList<String> historylist = new ArrayList<String>();
private String path;


    /**
     *
     * @param file_path specify file path for undo/save list
     */
    public UndoSave( String file_path ) {

        path = file_path;

    }

    /**
     * empty constructor
     */
    public UndoSave(  ) {

    }

    /**
     *
     * @param path sets path for undo/save list
     */
    public void setPath(String path){
        this.path = path;
    }

    /**
     *
     * @param element element to add to list
     */
    public void addToList(String element){
        list.add(element);
    }

    /**
     * clear the list
     */
    public void clearList(){
        list.clear();
    }

    /**
     * writes to undo/save file, formatted
     * @throws IOException throws excepts with didnt work
     */
    public void writeToFile() throws IOException {
        FileWriter write = new FileWriter( path );
        PrintWriter print_line = new PrintWriter( write );
        for (String line:list) {
            print_line.printf( "%s" + "%n" , line);
        }

        print_line.close();
    }

    /**
     * removes the last element from the list and adds removed element to history list
     */
    public void undo(){
        if(list.size() != 0) {
            historylist.add(list.get(list.size()-1));
            list.remove(list.size()-1);
        }
    }

    /**
     * adds history list element back to list so it can be redrawn then removes the same element
     */
    public void redo(){
        if(historylist.size() != 0) {
            list.add(historylist.get(historylist.size()-1));
            historylist.remove(historylist.size()-1);
        }
    }

}
