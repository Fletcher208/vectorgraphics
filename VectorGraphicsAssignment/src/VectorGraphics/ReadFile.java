package VectorGraphics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class ReadFile {
private JavaFXDrawing drawing = new JavaFXDrawing();

    /**
     *  Reads the file given to it and split the lines by the spaces between the words, it then separates the splitline array into the shape and the values
     *  after it translates the shape value from percentages to the canvas size in coordinate values by multiplying the value by the canvas width or height
     *  it then draws the shapes onto the canvas
     * @param filePath  where the file is being read from
     * @param GC    the canvas
     * @param undosave adds the save/undo list so it can be used
     */
    public void readLines(String filePath, GraphicsContext GC, UndoSave undosave){
    BufferedReader reader;

		try {
		    //sets up the reader
        reader = new BufferedReader(new FileReader(filePath));
        String line = reader.readLine();
        // clears the save/undo list
            undosave.clearList();
        // clears the canvas
            GC.clearRect(0,0,GC.getCanvas().getWidth(),GC.getCanvas().getHeight());
        String[] splitLine;
            ArrayList<Double> shapeValue = new ArrayList<Double>();
            //sets default colours
            GC.setFill(Color.TRANSPARENT);
            GC.setStroke(Color.BLACK);
        while (line != null) {
            // splits the text files lines into a array
            splitLine = line.split("\\s+");
            shapeValue = new ArrayList<>();
            // multiply's the percentage values to get x,y coordinates
            for (int i = 1; i < splitLine.length; i++) {
                if(!splitLine[0].equalsIgnoreCase("pen") && !splitLine[0].equalsIgnoreCase("fill")) {
                    if( i == 1 || i == 3){
                        shapeValue.add((Double.parseDouble(splitLine[i])*GC.getCanvas().getWidth() ));
                    }
                    if(i == 2 || i == 4){
                        shapeValue.add((Double.parseDouble(splitLine[i])*GC.getCanvas().getHeight() ));

                    }
                }
            }

                // changes fill colour
                if(splitLine[0].equalsIgnoreCase("fill")){
                    if(splitLine[1].equalsIgnoreCase("off")){
                        GC.setStroke(Color.BLACK);
                    }
                    else{
                        GC.setFill(Color.web(splitLine[1]));
                    }
                }
                // changes stroke colour
                if(splitLine[0].equalsIgnoreCase("pen")){
                    if(splitLine[1].equalsIgnoreCase("off")){
                        GC.setStroke(Color.TRANSPARENT);
                    }
                    else{
                        GC.setStroke(Color.web(splitLine[1]));
                    }

                }

                //draws rectangle with given shape values
                if(splitLine[0].equalsIgnoreCase("RECTANGLE")) {
                    drawing.readRectangle(shapeValue, GC);
                }
                //draws Line with given shape values
                if(splitLine[0].equalsIgnoreCase("Line")){
                    drawing.readLine(shapeValue,GC);
                }
                //draws ellipse with given shape values
                if(splitLine[0].equalsIgnoreCase("ellipse")){
                     drawing.readEllipse(shapeValue,GC);
                }
                //draws Plot with given shape values
                if(splitLine[0].equalsIgnoreCase("Plot")){
                    drawing.readPlot(shapeValue,GC);
                }
                // adds the new loaded shapes to the undo/save list
                undosave.addToList(line);
                // read next line
                line = reader.readLine();
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
