module VectorGraphicsAssignment {
    requires javafx.fxml;
    requires javafx.controls;
    requires java.desktop;
    requires org.junit.jupiter.api;

    opens VectorGraphics;
}